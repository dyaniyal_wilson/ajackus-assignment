
(function ($) {
    "use strict";
  
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(e){
        var check = true;
        let data = {};

        // User's post data object
        data.email = $('#email').val();
        data.first_name = $('#first_name').val()
        data.last_name = $('#last_name').val()
        data.phone_number = $('#phone_number').val();
        data.message = $('#message').val();

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if (check) {
          $("div.spanner").addClass("show");
          $("div.overlay").addClass("show");

          $.ajax({
            type: "POST",
            url: '/users',
            data: {users: data}
          }).success(function (data, status) {
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");

            if (data.status == 'success') {
              $('.input100').val('');
              swal({
                title: data.message,
                icon: "success",
                button: "close",
              });
            } else {
              swal({
                title: data.error,
                icon: "error",
                button: "close",
              });
            }
          })
        }

        return false;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    $('.select_locale').on('change', function() {
      window.location.href = '?locale=' + $(this).val() 
    })

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    

})(jQuery);