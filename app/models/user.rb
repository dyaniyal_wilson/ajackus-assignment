class User < ApplicationRecord
  validates :first_name, :last_name, 
    presence: true, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters" }

  validates :phone_number, 
            numericality: true,
            uniqueness: true, 
            length: { is: 10 }, 
            if: Proc.new { |num| num.phone_number.present? }
  
  validates :email,
            presence: true,
            uniqueness: true,
            format: { 
              with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i 
            }

  def name
    first_name + ' ' + last_name            
  end          
end
