class UsersController < ApplicationController
  # Skip Authenticity Token for API calls
  skip_before_action :verify_authenticity_token, only: :create

  def contact
  end

  def create
    user = User.new
    user.assign_attributes(users_params) 
    if user.save
      WelcomeMailer.with(user: user).welcome_email.deliver_now
      render json: { message: 'Message sent successfully', status: 'success'}  
    else

      render json: { error: user.errors.full_messages.first, status: 'failure' }
    end 
  end

  private

  def users_params
    params.require(:users).permit!
  end

end