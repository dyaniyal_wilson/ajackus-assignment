class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  around_action :switch_locale

  private

  def switch_locale(&action)
    params_locale = I18n.available_locales.include?(params[:locale].try(:to_sym)) && params[:locale]
    locale = params_locale || I18n.default_locale
    I18n.with_locale(locale, &action)
  end
end
