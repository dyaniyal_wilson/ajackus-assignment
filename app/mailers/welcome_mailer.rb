class WelcomeMailer < ApplicationMailer

  default from: 'Ajackus Consultant <jzbscdlhwknfzwrpky@twzhhq.online>'

  DEFAULT_TO_EMAIL = 'info@ajackus.com' # Default To Email is configurable 

  def welcome_email
    @user = params[:user]
    mail(to: DEFAULT_TO_EMAIL, subject: "Customer Query from #{@user.name}")
  end
end
