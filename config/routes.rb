Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users, only: :create, defaults: { format: 'json' }
  match "*path", to: "users#contact", via: :all
  
  root 'users#contact'
end
